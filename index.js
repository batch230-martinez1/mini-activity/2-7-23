// Activity instructions
// 1. Using and argument, parameter, and return keyword, get the value of 'isSuccessful' variable
// 2. Store('verb') the invocation to a variable 'paymentResult' (by storing the invocation you are storing the return statement result to a variable)

let downPaymentPrice = 2000;

function checkPayment(payment){ // >> A parameter is already provided
    // The value from the argument is passed to the parameter and will be evaluated by the conditonal statement below (if and else. The 'else' block will only run if the condition from the 'if' statement is not satisfied / evaluated false)
    isSuccessful = undefined;
    if(payment < downPaymentPrice){
      console.log(`A minimum payment of ${downPaymentPrice} is required`)  
      isSuccessful = false;
        // >> To make the value of 'isSuccessful' usable outside the function, pass it through a return keyword. 
        // >> Create a return statement here...
      return isSuccessful; 
        
    }
    else{
        let change = payment - downPaymentPrice;
        console.log("Your change is "+ change);
        isSuccessful = true;
        
         // >> To make the value of 'isSuccessful' usable outside the function, pass it through a return keyword. 
        // >> Create a return statement here...
        return isSuccessful;
    }
}
let paymentResult = checkPayment(1500);
console.log(paymentResult);
/*
    Documentation:
    // Variables and values initialized inside the function are not accessible outside the function (because it is not within its function scope)
    // console.log(isSuccessful); 
            // This will result to an error
*/
   
// A function will not run if not is not called or invoked.     
// >> Create your invocation, the invocation must be stored to a variable 'paymentResult' with a provided number as an argument
// >> Display the value of 'paymentResult' through console.log()

        
